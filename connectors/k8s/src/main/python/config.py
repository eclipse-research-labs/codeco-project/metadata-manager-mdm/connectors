"""
// SPDX-License-Identifier: Apache-2.0
// Copyright IBM Corp. 2023, 2024
"""

import yaml
import sys
import os

args = sys.argv[1:]




if "--env" in args:
    # Get all environment variables instat of value.yaml
    UNIQUE_CONNECTOR_NAME="KUBERNETS CONNECTOR"
    K8S_SA_TOKEN=os.getenv('K8S_SA_TOKEN')
    K8S_API_URL=os.getenv('K8S_API_URL')
    K8S_NAME=os.getenv('K8S_NAME')

    CONNECTOR_STATE="local"
    K8S_TOKEN_AUTH ="True"
    

else:
    yamlCfg = {}
    with open("config/application.yaml", "r") as stream:
        try:
            #print(yaml.safe_load(stream))
            yamlCfg = yaml.safe_load(stream)
            print(yamlCfg)
        except yaml.YAMLError as exc:
            print(exc)
    
    # Get other parameters from YAML in src/main/helm/values.yaml
    
    UNIQUE_CONNECTOR_NAME=yamlCfg["pathfinder"]["connector"]["id"]
    K8S_API_URL=yamlCfg["crawler"]["k8s"]["api_url"]
    K8S_EXTERNAL=True
    if "sa_token" in yamlCfg["crawler"]["k8s"]:
        K8S_SA_TOKEN=yamlCfg["crawler"]["k8s"]["sa_token"]
    else:
        with open('/var/run/secrets/kubernetes.io/serviceaccount/token', 'r') as f:
            K8S_SA_TOKEN = f.read()
            K8S_EXTERNAL=False
    if "name" in yamlCfg["crawler"]["k8s"]:
        K8S_NAME=yamlCfg["crawler"]["k8s"]["name"]


    # Get other parameters from YAML in src/main/helm/values.yaml
