"""
// SPDX-License-Identifier: Apache-2.0
// Copyright IBM Corp. 2023, 2024
"""

import uuid
import time
import logging
import sys
import json
import ast

import kubernetes
from kubernetes import client, config
from kubernetes.client.rest import ApiException
from kubernetes.client.models.v1_pod_list import V1PodList
from kubernetes.client.models.v1_pod import V1Pod
from kubernetes.client.models.v1_pod_spec import V1PodSpec
from kubernetes.client.models.v1_pod_status import V1PodStatus
from kubernetes.client.models.v1_node import V1Node

from kubernetes.client.models.v1_event_source import V1EventSource

import copy
from threading import Thread
import threading
import concurrent.futures

import config
from ibm_pathfinder_sdk import eventpublisher as msgObject, pathfinderconfig, pfmodelclasses
import mdmmodelclasses as pathfinderClass


class NULL_NAMESPACE:
    bytes = b''


def reportPodToPublisher(pod: V1Pod, pvcs: str, clusterName: str,
                         eventType: str,
                         mdmEventObj: pfmodelclasses.SystemEvent,
                         mdmPublishObj: msgObject.ConMsgObjectModelRegistry):
    # edf_id for pod
    # "pod:<clusterName>:<pod name>"
    podEdfId = (str(
        uuid.uuid3(
            NULL_NAMESPACE,
            str("pod:" + clusterName + ":" + pod.metadata.namespace + ":" +
                str(pod.metadata.name)))))

    podObj = pathfinderClass.Pod(podEdfId)
    podObj.setName(pod.metadata.name)
    podObj.setNamespace(pod.metadata.namespace)
    #podObj.setDescription(str(event['object']))
    podObj.setStatus(str(pod.status))
    podObj.setSubtype('CONTAINER')
    podObj.setPersistentVolumes(pvcs)

    mdmEventObj.payload = podObj
    if (eventType == "ADDED") or (eventType == "MODIFIED"):
        mdmEventObj.event_type = "upsert"
    else:
        mdmEventObj.event_type = "delete"
    mdmPublishObj.publishEvent(mdmEventObj)

    print(pod.spec.node_name)

    # Runs on Workernode
    if pod.spec.node_name:
        nodeEdfId = (str(
            uuid.uuid3(
                NULL_NAMESPACE,
                str("node:" + clusterName + ":" + str(pod.spec.node_name)))))

        print(nodeEdfId)

        containerRunsWorker = pathfinderClass.Runs(podEdfId, nodeEdfId)
        mdmEventObj.payload = containerRunsWorker
        mdmPublishObj.publishEvent(mdmEventObj)

    if pod.status.pod_ip:
        # Pod IP is Internal
        # internal --> edf_id=<clusterName>:<ip address>
        netEdfId = (str(
            uuid.uuid3(NULL_NAMESPACE,
                       str(clusterName + ":" + str(pod.status.pod_ip)))))
        net = pathfinderClass.Net_location(netEdfId)

        net.setHost(pod.status.pod_ip)
        net.setDomain("InternalIP")
        mdmEventObj.payload = net
        mdmPublishObj.publishEvent(mdmEventObj)

        runs = pathfinderClass.Runs(podEdfId, netEdfId)
        mdmEventObj.payload = runs
        mdmPublishObj.publishEvent(mdmEventObj)
    pass


def thredReportPods(core_v1: client.CoreV1Api,
                    mdmEventObj: pfmodelclasses.SystemEvent,
                    mdmPublishObj: msgObject.ConMsgObjectModelRegistry):
    clusterEdfId = str(uuid.uuid3(NULL_NAMESPACE, str(config.K8S_NAME)))
    pods = core_v1.list_pod_for_all_namespaces(watch=False)
    resource_version = pods.metadata.resource_version
    stream = kubernetes.watch.Watch().stream(
        func=core_v1.list_pod_for_all_namespaces,
        resource_version=resource_version,
        allow_watch_bookmarks=True)
    for pod in pods.items:
        pvcs = []
        for volume in pod.spec.volumes:
            if volume.persistent_volume_claim:
                pvc_name = volume.persistent_volume_claim.claim_name
                pvc = core_v1.read_namespaced_persistent_volume_claim(
                    name=pvc_name, namespace=pod.metadata.namespace)
                storage_class_name = pvc.spec.storage_class_name
                pvcs.append({
                    "pvc_name": pvc_name,
                    "storage_class_name": storage_class_name
                })
        reportPodToPublisher(pod, json.dumps(pvcs), str(config.K8S_NAME),
                             "ADDED", mdmEventObj, mdmPublishObj)
    i = 0
    while True:
        try:
            for event in stream:
                print(type(event["object"]))
                if kubernetes.client.models.v1_pod.V1Pod == type(
                        event["object"]):
                    logging.info(
                        "@@@!!!Kubernetes Event: %s %s" %
                        (event['type'], event['object'].metadata.name))
                    pvcs = []
                    for volume in event['type'].spec.volumes:
                        if volume.persistent_volume_claim:
                            pvc_name = volume.persistent_volume_claim.claim_name
                            pvc = core_v1.read_namespaced_persistent_volume_claim(
                                name=pvc_name,
                                namespace=event['type'].metadata.namespace)
                            storage_class_name = pvc.spec.storage_class_name
                            pvcs.append({
                                "pvc_name":
                                pvc_name,
                                "storage_class_name":
                                storage_class_name
                            })
                    reportPodToPublisher(event['object'], json.dumps(pvcs),
                                         str(config.K8S_NAME), event["type"],
                                         mdmEventObj, mdmPublishObj)

                    #print("first node event pubished")
                    #resource_version = pods.metadata.resource_version
                    resource_version = event[
                        'object'].metadata.resource_version
                    i = i + 1
                print("   -->   " + str(i))
        except ApiException as e:
            logging.error("thredReportPods breaks !!!")
            logging.error(e)
            logging.error(resource_version)
            logging.error(e.body)
            logging.error(e.args)
            logging.error(e.reason)
            logging.error(e.status)
            a1 = str(e.reason).find("(")
            a2 = str(e.reason).find(")")

            reasonText = str(e.reason)
            resource_version = int(reasonText[reasonText.find("(") +
                                              1:reasonText.find(")")])
            #exit(0)
            stream = kubernetes.watch.Watch().stream(
                func=core_v1.list_pod_for_all_namespaces,
                resource_version=resource_version,
                allow_watch_bookmarks=True)
            logging.error("Stream reloaded")
            pass


def reportNodeToPublisher(node: V1Node, clusterName: str, eventType: str,
                          mdmEventObj: pfmodelclasses.SystemEvent,
                          mdmPublishObj: msgObject.ConMsgObjectModelRegistry):
    nodeEdfId = (str(
        uuid.uuid3(NULL_NAMESPACE,
                   str("node:" + clusterName + ":" +
                       str(node.metadata.name)))))
    workerNode = pathfinderClass.Workernode(nodeEdfId)

    workerNode.setName(node.metadata.name)
    #node.setDescription(str(event['object']))
    workerNode.setDescription(str(node.status.node_info))
    workerNode.setStatus(str(node.status))
    mdmEventObj.payload = workerNode
    # set Event_type
    if (eventType == "ADDED") or (eventType == "MODIFIED"):
        mdmEventObj.event_type = "upsert"
    else:
        mdmEventObj.event_type = "delete"
    mdmPublishObj.publishEvent(mdmEventObj)

    # Contains in a Cluster
    clusterEdfId = str(uuid.uuid3(NULL_NAMESPACE, str(config.K8S_NAME)))
    contains = pathfinderClass.Contains(clusterEdfId, nodeEdfId)
    mdmEventObj.payload = contains
    mdmPublishObj.publishEvent(mdmEventObj)

    adresses = node.status.addresses
    for address in adresses:

        # don't report type Hostname --> Hostname == InternalIP
        # Net_Location edf_id
        # external --> edf_id=<ip address>
        # internal --> edf_id=<clusterEdfId>:<ip address>
        if (address.type == "InternalIP") or (address.type == "ExternalIP"):
            if address.type == "InternalIP":
                netEdfId = (str(
                    uuid.uuid3(NULL_NAMESPACE,
                               str(clusterName + ":" + address.address))))
            if address.type == "ExternalIP":
                netEdfId = str(uuid.uuid3(NULL_NAMESPACE,
                                          str(address.address)))
            net = pathfinderClass.Net_location(netEdfId)
            net.setDomain = address.type
            net.setHost = address.address
            mdmEventObj.payload = net
            mdmPublishObj.publishEvent(mdmEventObj)
            runs = pathfinderClass.Runs(netEdfId, nodeEdfId)
            mdmEventObj.payload = runs
            mdmPublishObj.publishEvent(mdmEventObj)

    pass


def thredReportWorkerNode(core_v1: client.CoreV1Api,
                          mdmEventObj: pfmodelclasses.SystemEvent,
                          mdmPublishObj: msgObject.ConMsgObjectModelRegistry):
    clusterEdfId = str(uuid.uuid3(NULL_NAMESPACE, str(config.K8S_NAME)))
    nodes = core_v1.list_node(watch=False)
    resource_version = nodes.metadata.resource_version
    stream = kubernetes.watch.Watch().stream(func=core_v1.list_node,
                                             resource_version=resource_version,
                                             allow_watch_bookmarks=True)

    for node in nodes.items:
        reportNodeToPublisher(node, config.K8S_NAME, "ADDED", mdmEventObj,
                              mdmPublishObj)

    while True:
        try:

            for event in stream:
                if kubernetes.client.models.v1_node.V1Node == type(
                        event["object"]):
                    reportNodeToPublisher(node, config.K8S_NAME, event["type"],
                                          mdmEventObj, mdmPublishObj)
                    pass
                pass
        except ApiException as e:
            logging.error("thredReportNodes breaks !!!")
            logging.error(e)

            stream = kubernetes.watch.Watch().stream(
                func=core_v1.list_node,
                resource_version=resource_version,
                allow_watch_bookmarks=True)
            pass
