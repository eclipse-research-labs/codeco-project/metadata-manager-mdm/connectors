"""
// SPDX-License-Identifier: Apache-2.0
// Copyright IBM Corp. 2023, 2024
"""

import uuid
import time
import logging
import sys
import json

import kubernetes
from kubernetes import client, config
from kubernetes.client.rest import ApiException

from threading import Thread
import k8swatcher
import config
from ibm_pathfinder_sdk import eventpublisher as msgObject, pathfinderconfig, pfmodelclasses

import mdmmodelclasses as pathfinderClass
# for e.g. other model
#from qsadvisormodelclasses import pathfinderClass


class NULL_NAMESPACE:
    bytes = b''


def connectorDefiniton():
    # define unique connector name for edf_id
    connectorUuid = str(
        uuid.uuid3(NULL_NAMESPACE, config.UNIQUE_CONNECTOR_NAME)
    ) if pathfinderconfig.UNIQUE_CONNECTOR_ID == "" else str(
        uuid.uuid3(NULL_NAMESPACE, pathfinderconfig.UNIQUE_CONNECTOR_ID))
    connectorType = "PYTHON"
    event = pfmodelclasses.SystemEvent(connectorUuid, connectorType)
    event.timestamp = time.time()
    return event


# check the connection to all endpoints
# Return 0 all ok, otherwise report(logging) and Return 1
def checkConnection():
    logging.info("check connections")

    return 0


def main():
    args = sys.argv[1:]
    if "--test" in args:
        if checkConnection() > 0:
            exit(1)
        else:
            exit(0)

    event = connectorDefiniton()

    # use pf-model-registry
    conMsgObject = msgObject.ConMsgObjectModelRegistry()
    #conMsgObject =  msgObject.ConMsgObjectBase()
    # use pf-model-registry
    #conMsgObject =  msgObject.ConMsgObjectKafka()

    # load last state from persistant volume
    #conMsgObject.loadLastState()
    #print(pathfinderconfig.PATHFINDER_CONNECTOR_STOP_SIGNAL)
    #if pathfinderconfig.PATHFINDER_CONNECTOR_STOP_SIGNAL.upper() == 'GO':
    if 1 == 1:

        aToken = config.K8S_SA_TOKEN
        print("#################################")
        aConfiguration = client.Configuration()
        if config.K8S_EXTERNAL:
            aConfiguration.host = config.K8S_API_URL
        else:
            aConfiguration.host = "https://kubernetes.default.svc"
        aConfiguration.verify_ssl = False
        aConfiguration.api_key = {"authorization": "Bearer " + aToken}
        aApiClient = client.ApiClient(aConfiguration)
        core_v1 = client.CoreV1Api(aApiClient)

        clusterEdfId = str(uuid.uuid3(NULL_NAMESPACE, str(config.K8S_NAME)))
        cluster = pathfinderClass.Kubernetes(clusterEdfId)
        cluster.setType("KUBERNETES")
        cluster.setSubtype("")
        cluster.setUrl(config.K8S_API_URL)

        #read config map cluster-info -> namespace kube-public or kube-system
        clusterInfo = None
        try:
            clusterInfo = core_v1.read_namespaced_config_map(
                namespace="kube-system", name="cluster-info")
        except:
            try:
                clusterInfo = core_v1.read_namespaced_config_map(
                    namespace="kube-public", name="cluster-info")
            except:
                pass
        cluster.setDescription(str(clusterInfo))

        storagev1 = client.StorageV1Api(aApiClient)
        sc = storagev1.list_storage_class()
        storageClasses = []
        for s in sc.items:
            storageClasses.append(s.metadata.name)
        cluster.setStorageClasses(storageClasses)

        if (config.K8S_NAME):
            cluster.setName(config.K8S_NAME)
        event.payload = cluster
        conMsgObject.publishEvent(event)

        threads = []

        trp = Thread(target=k8swatcher.thredReportPods,
                     args=(
                         core_v1,
                         event,
                         conMsgObject,
                     ))
        threads.append(trp)
        trp.start()
        trw = Thread(target=k8swatcher.thredReportWorkerNode,
                     args=(
                         core_v1,
                         event,
                         conMsgObject,
                     ))
        threads.append(trw)
        trw.start()

        # code will never come to this point, watch runs for ever
        trp.join()
        trw.join()

        exit(0)


main()
