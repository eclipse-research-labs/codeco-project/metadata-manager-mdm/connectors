"""
// SPDX-License-Identifier: Apache-2.0
// Copyright IBM Corp. 2024
"""

import uuid
import time
import logging
import sys
import json
import time

import config
from ibm_pathfinder_sdk import eventpublisher as msgObject, pathfinderconfig, pfmodelclasses

import requests

import mdmmodelclasses as pathfinderClass
# for e.g. other model
#from qsadvisormodelclasses import pathfinderClass


class NULL_NAMESPACE:
    bytes = b''


def connectorDefiniton():
    # define unique connector name for edf_id
    connectorUuid = str(
        uuid.uuid3(NULL_NAMESPACE, config.UNIQUE_CONNECTOR_NAME)
    ) if pathfinderconfig.UNIQUE_CONNECTOR_ID == "" else str(
        uuid.uuid3(NULL_NAMESPACE, pathfinderconfig.UNIQUE_CONNECTOR_ID))
    connectorType = "PYTHON"
    event = pfmodelclasses.SystemEvent(connectorUuid, connectorType)
    event.timestamp = time.time()
    return event


# check the connection to all endpoints
# Return 0 all ok, otherwise report(logging) and Return 1
def checkConnection():
    logging.info("check connections")

    return 0


def main():
    args = sys.argv[1:]
    if "--test" in args:
        if checkConnection() > 0:
            exit(1)
        else:
            exit(0)

    event = connectorDefiniton()
    conMsgObject = msgObject.ConMsgObjectModelRegistry()

    while True:
        print("loop")

        response = requests.get(config.PROMETHEUS_URL + ":" +
                                str(config.PROMETHEUS_PORT) + "/api/v1/query",
                                params={
                                    "query":
                                    config.PROMETHEUS_METRIC +
                                    "{kubernetes_pod_name!=\'\'}"
                                })

        if response.status_code == 200:
            for metricResult in response.json()["data"]["result"]:
                appName = metricResult["metric"]["app"]
                podName = metricResult["metric"]["kubernetes_pod_name"]
                nameSpace = metricResult["metric"]["kubernetes_namespace"]
                metricValue = metricResult["value"][1]
                metricEdfId = str(
                    uuid.uuid3(NULL_NAMESPACE,
                               str(config.PROMETHEUS_METRIC + ":" + podName)))

                class_ = getattr(pathfinderClass, config.ENTITY_NAME)
                instance = class_(metricEdfId)
                instance.setApp(appName)
                instance.setPodName(podName)
                instance.setNamespace(str(nameSpace))
                instance.setValue(str(metricValue))

                event.payload = instance
                conMsgObject.publishEvent(event)
                #podEdfId = str(uuid.uuid3(NULL_NAMESPACE,str(podName)))
                podEdfId = (str(
                    uuid.uuid3(
                        NULL_NAMESPACE,
                        str("pod:" + config.K8S_NAME + ":" + nameSpace + ":" +
                            str(podName)))))

                has = pathfinderClass.Has(podEdfId, metricEdfId)
                event.payload = has
                conMsgObject.publishEvent(event)
        else:
            print(response)

        time.sleep(int(config.PROMETHEUS_RERUN))


main()
