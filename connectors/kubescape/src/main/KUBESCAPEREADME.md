<!--
  ~ SPDX-License-Identifier: Apache-2.0
  ~ Copyright IBM Corp 2023
-->

# mdm-connector

## build docker image k8s connector
from project root directory
```
docker build -t hecodeco/mdm-connector-kubescape:<VERSION TAG from HELM VERSION> -f connectors/kubescape/src/main/docker/Dockerfile .
docker push hecodeco/mdm-connector-kubescape:<VERSION TAG from HELM VERSION>
```



