"""
// SPDX-License-Identifier: Apache-2.0
// Copyright IBM Corp. 2024
"""

import yaml
import sys
import os

args = sys.argv[1:]

if "--env" in args:
    # Get all environment variables instat of value.yaml
    UNIQUE_CONNECTOR_NAME = "KUBESCAPE CONNECTOR"
    K8S_SA_TOKEN = os.getenv('K8S_SA_TOKEN')
    K8S_API_URL = os.getenv('K8S_API_URL')

    CONNECTOR_STATE = "local"
    K8S_TOKEN_AUTH = "True"
    KUBESCAPE_RERUN = int(os.getenv('KUBESCAPE_RERUN'), default=300)
    K8S_NAME = os.getenv('K8S_NAME')

else:
    yamlCfg = {}
    with open("config/application.yaml", "r") as stream:
        try:
            #print(yaml.safe_load(stream))
            yamlCfg = yaml.safe_load(stream)
            print(yamlCfg)
        except yaml.YAMLError as exc:
            print(exc)

    # Get other parameters from YAML in src/main/helm/values.yaml

    UNIQUE_CONNECTOR_NAME = yamlCfg["pathfinder"]["connector"]["id"]
    K8S_API_URL = yamlCfg["crawler"]["k8s"]["api_url"]
    if "name" in yamlCfg["crawler"]["k8s"]:
        K8S_NAME = yamlCfg["crawler"]["k8s"]["name"]
    if "sa_token" in yamlCfg["crawler"]["k8s"]:
        K8S_SA_TOKEN = yamlCfg["crawler"]["k8s"]["sa_token"]
    else:
        with open('/var/run/secrets/kubernetes.io/serviceaccount/token',
                  'r') as f:
            K8S_SA_TOKEN = f.read()
    if "kubescape_rerun" in yamlCfg["crawler"]:
        KUBESCAPE_RERUN = int(yamlCfg["crawler"]["kubescape_rerun"],
                              default=300)
    else:
        KUBESCAPE_RERUN = 300

    # Get other parameters from YAML in src/main/helm/values.yaml
