"""
// SPDX-License-Identifier: Apache-2.0
// Copyright IBM Corp. 2024
"""

import uuid
import time
import logging
import sys
import json
import time

import config
from ibm_pathfinder_sdk import eventpublisher as msgObject, pathfinderconfig, pfmodelclasses

import subprocess

import mdmmodelclasses as pathfinderClass
# for e.g. other model
#from qsadvisormodelclasses import pathfinderClass


class NULL_NAMESPACE:
    bytes = b''


def connectorDefiniton():
    # define unique connector name for edf_id
    connectorUuid = str(
        uuid.uuid3(NULL_NAMESPACE, config.UNIQUE_CONNECTOR_NAME)
    ) if pathfinderconfig.UNIQUE_CONNECTOR_ID == "" else str(
        uuid.uuid3(NULL_NAMESPACE, pathfinderconfig.UNIQUE_CONNECTOR_ID))
    connectorType = "PYTHON"
    event = pfmodelclasses.SystemEvent(connectorUuid, connectorType)
    event.timestamp = time.time()
    return event


# check the connection to all endpoints
# Return 0 all ok, otherwise report(logging) and Return 1
def checkConnection():
    logging.info("check connections")

    return 0


def main():
    args = sys.argv[1:]
    if "--test" in args:
        if checkConnection() > 0:
            exit(1)
        else:
            exit(0)

    event = connectorDefiniton()
    conMsgObject = msgObject.ConMsgObjectModelRegistry()

    args = ("/usr/local/bin/kubescape", "scan", "--format", "json",
            "--format-version", "v2", "--output", "/tmp/results.json")

    while True:
        popen = subprocess.Popen(args, stdout=subprocess.PIPE)
        popen.wait()
        output = popen.stdout.read()

        f = open('/tmp/results.json')
        scanResults = json.load(f)
        complianceScore = scanResults["summaryDetails"]["complianceScore"]

        clusterEdfId = str(uuid.uuid3(NULL_NAMESPACE, str(config.K8S_NAME)))
        complianceEdfId = str(
            uuid.uuid3(NULL_NAMESPACE,
                       str("Compliance_state:" + config.K8S_NAME)))
        compliance = pathfinderClass.Compliance_state(complianceEdfId)
        compliance.setUrl_source_compliance_system(config.K8S_NAME)
        compliance.setSource_compliance_system("kubescape")
        compliance.setStatus(complianceScore)

        event.payload = compliance
        conMsgObject.publishEvent(event)

        complies = pathfinderClass.Complies(clusterEdfId, complianceEdfId)
        event.payload = complies
        conMsgObject.publishEvent(event)
        time.sleep(int(config.KUBESCAPE_RERUN))


main()
