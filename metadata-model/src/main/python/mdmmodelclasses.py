"""
// SPDX-License-Identifier: Apache-2.0
// Copyright IBM Corp. 2023
"""
import json
from ibm_pathfinder_sdk import pfmodelclasses


class Ai_model(pfmodelclasses.SystemEntity):
    """
	AI model entity
	"""

    def __init__(self, edf_id):
        self.json_schema_ref = "urn:eu.codecohe:ai_model:1.0.0"
        self.edf_id = edf_id

    def setAlgorithm(self, algorithm):
        """
		 type        string
		 description Description of training algorithm(s) such as DNN, random forest, gradient boost, etc.
		"""
        self.algorithm = algorithm

    def setBuild_type(self, build_type):
        """
		 type        string
		 description Description reflecting the build environment. Examples are 'IBM autoai', 'custom', ' Azure ML', etc.
		"""
        self.build_type = build_type

    def setFactsheet_url(self, factsheet_url):
        """
		 type        string
		 description URL of additional descriptive information or metadata, such as a factsheet, relating to the model.
		"""
        self.factsheet_url = factsheet_url

    def setMetrics(self, metrics):
        """
		 type        array
		 description model metrics
		"""
        self.metrics = metrics

    def setName(self, name):
        """
		 type        string
		 description The (human readable) name of the entity
		"""
        self.name = name

    def setDescription(self, description):
        """
		 type        string
		 description A description of the entity. May be empty.
		"""
        self.description = description

    def setIdentifier(self, identifier):
        """
		 type        string
		 description Name or other identifier of this entity. May be empty.
		"""
        self.identifier = identifier

    def setUrl(self, url):
        """
		 type        string
		 description URL that is used to locate this entity. May be empty.
		"""
        self.url = url

    def setType(self, type):
        """
		 type        string
		 description The more detailed type of the entity, e.g. RDBMS for a DataStore entity
		"""
        self.type = type

    def setSubtype(self, subtype):
        """
		 type        string
		 description The more detailed subtype of the entity, e.g. DB2 for an RDBMS type DataStore entity
		"""
        self.subtype = subtype

    def setRegion(self, region):
        """
		 description The region.
		"""
        self.region = region

    def setCountry_code(self, country_code):
        """
		 type        string
		 description ISO 3166-1 alpha-2 two letter code
		"""
        self.country_code = country_code

    def setCity(self, city):
        """
		 type        string
		 description Optional name of the city or area
		"""
        self.city = city

    def setTags(self, tags):
        """
		 type        array
		 description An array of tags associated with this entity.
		"""
        self.tags = tags

    def toJSON(self):
        return json.dumps(self,
                          default=lambda o: o.__dict__,
                          sort_keys=True,
                          indent=4)


class Freshness_state(pfmodelclasses.SystemEntity):
    """
	freshness state entity
	"""

    def __init__(self, edf_id):
        self.json_schema_ref = "urn:eu.codecohe:freshness_state:1.0.0"
        self.edf_id = edf_id

    def setValue(self, value):
        """
		 type        string
		 description The value of the Freshness_state.
		"""
        self.value = value

    def setApp(self, appName):
        """
		 type        string
		 The app name / container name.
		"""
        self.app = appName

    def setPodName(self, podName):
        """
		 type        string
		 The namespace of the pod.
		"""
        self.name = podName

    def setNamespace(self, nameSpace):
        """
		 type        string
		 description The pod name of the Freshness_state.
		"""
        self.namespace = nameSpace


class Compliance_state(pfmodelclasses.SystemEntity):
    """
	compliance state entity
	"""

    def __init__(self, edf_id):
        self.json_schema_ref = "urn:eu.codecohe:compliance_state:1.0.0"
        self.edf_id = edf_id

    def setSource_compliance_system(self, source_compliance_system):
        """
		 type        string
		 description The source of the compliance state.
		"""
        self.source_compliance_system = source_compliance_system

    def setUrl_source_compliance_system(self, url_source_compliance_system):
        """
		 type        string
		 description The URL to the compliance system that produced the compliance state.
		"""
        self.url_source_compliance_system = url_source_compliance_system

    def setResource_name(self, resource_name):
        """
		 type        string
		 description The resource this complianceState belongs to.
		"""
        self.resource_name = resource_name

    def setResource_type(self, resource_type):
        """
		 type        string
		 description The type of the resource this complianceState belongs to.
		"""
        self.resource_type = resource_type

    def setRegulation(self, regulation):
        """
		 type        string
		 description The regulation under which the complianceState has been defined.
		"""
        self.regulation = regulation

    def setStatus(self, status):
        """
		 type        string
		 description The overall state of this compliance.
		"""
        self.status = status

    def setControls(self, controls):
        """
		 type        array
		"""
        self.controls = controls

    def setName(self, name):
        """
		 type        string
		 description The (human readable) name of the entity
		"""
        self.name = name

    def setDescription(self, description):
        """
		 type        string
		 description A description of the entity. May be empty.
		"""
        self.description = description

    def setIdentifier(self, identifier):
        """
		 type        string
		 description Name or other identifier of this entity. May be empty.
		"""
        self.identifier = identifier

    def setUrl(self, url):
        """
		 type        string
		 description URL that is used to locate this entity. May be empty.
		"""
        self.url = url

    def setType(self, type):
        """
		 type        string
		 description The more detailed type of the entity, e.g. RDBMS for a DataStore entity
		"""
        self.type = type

    def setSubtype(self, subtype):
        """
		 type        string
		 description The more detailed subtype of the entity, e.g. DB2 for an RDBMS type DataStore entity
		"""
        self.subtype = subtype

    def toJSON(self):
        return json.dumps(self,
                          default=lambda o: o.__dict__,
                          sort_keys=True,
                          indent=4)


class Datastore(pfmodelclasses.SystemEntity):
    """
	datastore entity
	"""

    def __init__(self, edf_id):
        self.json_schema_ref = "urn:eu.codecohe:datastore:1.1.0"
        self.edf_id = edf_id

    def setName(self, name):
        """
		 type        string
		 description The (human readable) name of the entity
		"""
        self.name = name

    def setDescription(self, description):
        """
		 type        string
		 description A description of the entity. May be empty.
		"""
        self.description = description

    def setIdentifier(self, identifier):
        """
		 type        string
		 description Name or other identifier of this entity. May be empty.
		"""
        self.identifier = identifier

    def setUrl(self, url):
        """
		 type        string
		 description URL that is used to locate this entity. May be empty.
		"""
        self.url = url

    def setType(self, type):
        """
		 type        string
		 description The more detailed type of the entity, e.g. RDBMS for a DataStore entity
		"""
        self.type = type

    def setSubtype(self, subtype):
        """
		 type        string
		 description The more detailed subtype of the entity, e.g. DB2 for an RDBMS type DataStore entity
		"""
        self.subtype = subtype

    def setClassification(self, classification):
        """
		 type        string
		 description The classification of the EDF.
		"""
        self.classification = classification

    def setClassification_value(self, classification_value):
        """
		 type        string
		 description An optional value of the classification
		"""
        self.classification_value = classification_value

    def setRegion(self, region):
        """
		 description The region.
		"""
        self.region = region

    def setCountry_code(self, country_code):
        """
		 type        string
		 description ISO 3166-1 alpha-2 two letter code
		"""
        self.country_code = country_code

    def setCity(self, city):
        """
		 type        string
		 description Optional name of the city or area
		"""
        self.city = city

    def setRegulation(self, regulation):
        """
		 type        array
		 description An array of regulations.
		"""
        self.regulation = regulation

    def setTags(self, tags):
        """
		 type        array
		 description An array of tags associated with this entity.
		"""
        self.tags = tags

    def setVersion(self, version):
        """
		 type        string
		 description The full version, can be a maven group/artifact/version string.
		"""
        self.version = version

    def setGit_commit_id(self, git_commit_id):
        """
		 type        string
		 description The git commit id.
		"""
        self.git_commit_id = git_commit_id

    def setGit_build_version(self, git_build_version):
        """
		 type        string
		 description The git build version.
		"""
        self.git_build_version = git_build_version

    def setVersion_url(self, version_url):
        """
		 type        string
		 description The URL where more information can be found, e.g. a doc webpage, a git repository.
		"""
        self.version_url = version_url

    def toJSON(self):
        return json.dumps(self,
                          default=lambda o: o.__dict__,
                          sort_keys=True,
                          indent=4)


class Data_asset(pfmodelclasses.SystemEntity):
    """
	data asset entity
	"""

    def __init__(self, edf_id):
        self.json_schema_ref = "urn:eu.codecohe:data_asset:1.1.0"
        self.edf_id = edf_id

    def setChange_rate(self, change_rate):
        """
		 type        integer
		 description The rate of change of a data asset, e.g. in records/sec
		"""
        self.change_rate = change_rate

    def setName(self, name):
        """
		 type        string
		 description The (human readable) name of the entity
		"""
        self.name = name

    def setDescription(self, description):
        """
		 type        string
		 description A description of the entity. May be empty.
		"""
        self.description = description

    def setIdentifier(self, identifier):
        """
		 type        string
		 description Name or other identifier of this entity. May be empty.
		"""
        self.identifier = identifier

    def setUrl(self, url):
        """
		 type        string
		 description URL that is used to locate this entity. May be empty.
		"""
        self.url = url

    def setType(self, type):
        """
		 type        string
		 description The more detailed type of the entity, e.g. RDBMS for a DataStore entity
		"""
        self.type = type

    def setSubtype(self, subtype):
        """
		 type        string
		 description The more detailed subtype of the entity, e.g. DB2 for an RDBMS type DataStore entity
		"""
        self.subtype = subtype

    def setClassification(self, classification):
        """
		 type        string
		 description The classification of the EDF.
		"""
        self.classification = classification

    def setClassification_value(self, classification_value):
        """
		 type        string
		 description An optional value of the classification
		"""
        self.classification_value = classification_value

    def setRegion(self, region):
        """
		 description The region.
		"""
        self.region = region

    def setCountry_code(self, country_code):
        """
		 type        string
		 description ISO 3166-1 alpha-2 two letter code
		"""
        self.country_code = country_code

    def setCity(self, city):
        """
		 type        string
		 description Optional name of the city or area
		"""
        self.city = city

    def setPurpose(self, purpose):
        """
		 type        array
		 description An array of purposes.
		"""
        self.purpose = purpose

    def setRegulation(self, regulation):
        """
		 type        array
		 description An array of regulations.
		"""
        self.regulation = regulation

    def setTags(self, tags):
        """
		 type        array
		 description An array of tags associated with this entity.
		"""
        self.tags = tags

    def setColumns(self, columns):
        """
		 type        array
		 description Format description of all columns
		"""
        self.columns = columns

    def toJSON(self):
        return json.dumps(self,
                          default=lambda o: o.__dict__,
                          sort_keys=True,
                          indent=4)


class Failure(pfmodelclasses.SystemEntity):
    """
	A failure by a compute instance
	"""

    def __init__(self, edf_id):
        self.json_schema_ref = "urn:eu.codecohe:failure:1.0.0"
        self.edf_id = edf_id

    def setLog(self, log):
        """
		 type        string
		 description A log associated with this failure
		"""
        self.log = log

    def setName(self, name):
        """
		 type        string
		 description The (human readable) name of the entity
		"""
        self.name = name

    def setDescription(self, description):
        """
		 type        string
		 description A description of the entity. May be empty.
		"""
        self.description = description

    def setIdentifier(self, identifier):
        """
		 type        string
		 description Name or other identifier of this entity. May be empty.
		"""
        self.identifier = identifier

    def setUrl(self, url):
        """
		 type        string
		 description URL that is used to locate this entity. May be empty.
		"""
        self.url = url

    def setType(self, type):
        """
		 type        string
		 description The more detailed type of the entity, e.g. RDBMS for a DataStore entity
		"""
        self.type = type

    def setSubtype(self, subtype):
        """
		 type        string
		 description The more detailed subtype of the entity, e.g. DB2 for an RDBMS type DataStore entity
		"""
        self.subtype = subtype

    def setTags(self, tags):
        """
		 type        array
		 description An array of tags associated with this entity.
		"""
        self.tags = tags

    def toJSON(self):
        return json.dumps(self,
                          default=lambda o: o.__dict__,
                          sort_keys=True,
                          indent=4)


class Kubernetes(pfmodelclasses.SystemEntity):
    """
	Kubernetes cluster
	"""

    def __init__(self, edf_id):
        self.json_schema_ref = "urn:eu.codecohe:kubernetes:1.0.0"
        self.edf_id = edf_id

    def setName(self, name):
        """
		 type        string
		 description The (human readable) name of the entity
		"""
        self.name = name

    def setDescription(self, description):
        """
		 type        string
		 description A description of the entity. May be empty.
		"""
        self.description = description

    def setIdentifier(self, identifier):
        """
		 type        string
		 description Name or other identifier of this entity. May be empty.
		"""
        self.identifier = identifier

    def setUrl(self, url):
        """
		 type        string
		 description URL that is used to locate this entity. May be empty.
		"""
        self.url = url

    def setType(self, type):
        """
		 type        string
		 description The more detailed type of the entity, e.g. RDBMS for a DataStore entity
		"""
        self.type = type

    def setSubtype(self, subtype):
        """
		 type        string
		 description The more detailed subtype of the entity, e.g. DB2 for an RDBMS type DataStore entity
		"""
        self.subtype = subtype

    def setClassification(self, classification):
        """
		 type        string
		 description The classification of the EDF.
		"""
        self.classification = classification

    def setClassification_value(self, classification_value):
        """
		 type        string
		 description An optional value of the classification
		"""
        self.classification_value = classification_value

    def setRegion(self, region):
        """
		 description The region.
		"""
        self.region = region

    def setCountry_code(self, country_code):
        """
		 type        string
		 description ISO 3166-1 alpha-2 two letter code
		"""
        self.country_code = country_code

    def setCity(self, city):
        """
		 type        string
		 description Optional name of the city or area
		"""
        self.city = city

    def setTags(self, tags):
        """
		 type        array
		 description An array of tags associated with this entity.
		"""
        self.tags = tags

    def setStorageClasses(self, storage_classes):
        """
		 type        array
		 description An array of storagclasses with this entity.
		"""
        self.storage_classes = storage_classes

    def setVersion(self, version):
        """
		 type        string
		 description The full version, can be a maven group/artifact/version string.
		"""
        self.version = version

    def setGit_commit_id(self, git_commit_id):
        """
		 type        string
		 description The git commit id.
		"""
        self.git_commit_id = git_commit_id

    def setGit_build_version(self, git_build_version):
        """
		 type        string
		 description The git build version.
		"""
        self.git_build_version = git_build_version

    def setVersion_url(self, version_url):
        """
		 type        string
		 description The URL where more information can be found, e.g. a doc webpage, a git repository.
		"""
        self.version_url = version_url

    def toJSON(self):
        return json.dumps(self,
                          default=lambda o: o.__dict__,
                          sort_keys=True,
                          indent=4)


class Net_location(pfmodelclasses.SystemEntity):
    """
	A network location, can also specify a service endpoint.
	"""

    def __init__(self, edf_id):
        self.json_schema_ref = "urn:eu.codecohe:net_location:1.0.0"
        self.edf_id = edf_id

    def setHost(self, host):
        """
		 type        string
		 description host IP
		"""
        self.host = host

    def setPort(self, port):
        """
		 type        integer
		 description port number, optional
		"""
        self.port = port

    def setDomain(self, domain):
        """
		 type        string
		 description The domain distinguishes private network addresses.
		"""
        self.domain = domain

    def toJSON(self):
        return json.dumps(self,
                          default=lambda o: o.__dict__,
                          sort_keys=True,
                          indent=4)


class Performance(pfmodelclasses.SystemEntity):
    """
	Performance information for a pod
	"""

    def __init__(self, edf_id):
        self.json_schema_ref = "urn:eu.codecohe:performance:1.0.0"
        self.edf_id = edf_id

    def setProcess_rate(self, process_rate):
        """
		 type        integer
		 description The rate of processing of data by a pod, e.g. in records/sec
		"""
        self.process_rate = process_rate

    def setDuration(self, duration):
        """
		 type        integer
		 description The duration of the execution in sec., for example for a job
		"""
        self.duration = duration

    def setName(self, name):
        """
		 type        string
		 description The (human readable) name of the entity
		"""
        self.name = name

    def setDescription(self, description):
        """
		 type        string
		 description A description of the entity. May be empty.
		"""
        self.description = description

    def setIdentifier(self, identifier):
        """
		 type        string
		 description Name or other identifier of this entity. May be empty.
		"""
        self.identifier = identifier

    def setUrl(self, url):
        """
		 type        string
		 description URL that is used to locate this entity. May be empty.
		"""
        self.url = url

    def setType(self, type):
        """
		 type        string
		 description The more detailed type of the entity, e.g. RDBMS for a DataStore entity
		"""
        self.type = type

    def setSubtype(self, subtype):
        """
		 type        string
		 description The more detailed subtype of the entity, e.g. DB2 for an RDBMS type DataStore entity
		"""
        self.subtype = subtype

    def toJSON(self):
        return json.dumps(self,
                          default=lambda o: o.__dict__,
                          sort_keys=True,
                          indent=4)


class Pod(pfmodelclasses.SystemEntity):
    """
	A pod running a container
	"""

    def __init__(self, edf_id):
        self.json_schema_ref = "urn:eu.codecohe:pod:1.0.0"
        self.edf_id = edf_id

    def setImages(self, images):
        """
		 type        array
		 description The container images running in the pod.
		"""
        self.images = images

    def setNamespace(self, namespace):
        """
		 type        string
		 description The namespace where the pod runs
		"""
        self.namespace = namespace

    def setName(self, name):
        """
		 type        string
		 description The (human readable) name of the entity
		"""
        self.name = name

    def setDescription(self, description):
        """
		 type        string
		 description A description of the entity. May be empty.
		"""
        self.description = description

    def setIdentifier(self, identifier):
        """
		 type        string
		 description Name or other identifier of this entity. May be empty.
		"""
        self.identifier = identifier

    def setUrl(self, url):
        """
		 type        string
		 description URL that is used to locate this entity. May be empty.
		"""
        self.url = url

    def setType(self, type):
        """
		 type        string
		 description The more detailed type of the entity, e.g. RDBMS for a DataStore entity
		"""
        self.type = type

    def setSubtype(self, subtype):
        """
		 type        string
		 description The more detailed subtype of the entity, e.g. DB2 for an RDBMS type DataStore entity
		"""
        self.subtype = subtype

    def setClassification(self, classification):
        """
		 type        string
		 description The classification of the EDF.
		"""
        self.classification = classification

    def setClassification_value(self, classification_value):
        """
		 type        string
		 description An optional value of the classification
		"""
        self.classification_value = classification_value

    def setRegion(self, region):
        """
		 description The region.
		"""
        self.region = region

    def setCountry_code(self, country_code):
        """
		 type        string
		 description ISO 3166-1 alpha-2 two letter code
		"""
        self.country_code = country_code

    def setCity(self, city):
        """
		 type        string
		 description Optional name of the city or area
		"""
        self.city = city

    def setPurpose(self, purpose):
        """
		 type        array
		 description An array of purposes.
		"""
        self.purpose = purpose

    def setTags(self, tags):
        """
		 type        array
		 description An array of tags associated with this entity.
		"""
        self.tags = tags

    def setVersion(self, version):
        """
		 type        string
		 description The full version, can be a maven group/artifact/version string.
		"""
        self.version = version

    def setGit_commit_id(self, git_commit_id):
        """
		 type        string
		 description The git commit id.
		"""
        self.git_commit_id = git_commit_id

    def setGit_build_version(self, git_build_version):
        """
		 type        string
		 description The git build version.
		"""
        self.git_build_version = git_build_version

    def setVersion_url(self, version_url):
        """
		 type        string
		 description The URL where more information can be found, e.g. a doc webpage, a git repository.
		"""
        self.version_url = version_url

    def setStatus(self, status):
        """
		 type        string
		 description The status of the entity.
		"""
        self.status = status

    def setPersistentVolumes(self, persistent_volumes):
        """
		 type        string
		 description persistent_volumes array of {"pvc_name":"","storage_class":""} of the entity.
		"""
        self.persistent_volumes = persistent_volumes

    def toJSON(self):
        return json.dumps(self,
                          default=lambda o: o.__dict__,
                          sort_keys=True,
                          indent=4)


class Risk(pfmodelclasses.SystemEntity):
    """
	risk entity
	"""

    def __init__(self, edf_id):
        self.json_schema_ref = "urn:eu.codecohe:risk:1.0.0"
        self.edf_id = edf_id

    def setSource_system(self, source_system):
        """
		 type        string
		 description The system that identified the risk.
		"""
        self.source_system = source_system

    def setRisk_value(self, risk_value):
        """
		 type        string
		 description An optional risk value, level or some other risk quantification.
		"""
        self.risk_value = risk_value

    def setRegulation(self, regulation):
        """
		 type        string
		 description The optional regulation that defines the risk.
		"""
        self.regulation = regulation

    def setRegulation_subsection(self, regulation_subsection):
        """
		 type        string
		 description The optional regulation article or subsection that defines this risk.
		"""
        self.regulation_subsection = regulation_subsection

    def setReason(self, reason):
        """
		 type        string
		 description A description of the risk.
		"""
        self.reason = reason

    def setSeverity(self, severity):
        """
		 type        string
		 description An optional severity value of the risk.
		"""
        self.severity = severity

    def setSources_of_risk(self, sources_of_risk):
        """
		 type        array
		 description An array of EDF-IDs of entities that are involved in this risk.
		"""
        self.sources_of_risk = sources_of_risk

    def setName(self, name):
        """
		 type        string
		 description The (human readable) name of the entity
		"""
        self.name = name

    def setDescription(self, description):
        """
		 type        string
		 description A description of the entity. May be empty.
		"""
        self.description = description

    def setIdentifier(self, identifier):
        """
		 type        string
		 description Name or other identifier of this entity. May be empty.
		"""
        self.identifier = identifier

    def setUrl(self, url):
        """
		 type        string
		 description URL that is used to locate this entity. May be empty.
		"""
        self.url = url

    def setType(self, type):
        """
		 type        string
		 description The more detailed type of the entity, e.g. RDBMS for a DataStore entity
		"""
        self.type = type

    def setSubtype(self, subtype):
        """
		 type        string
		 description The more detailed subtype of the entity, e.g. DB2 for an RDBMS type DataStore entity
		"""
        self.subtype = subtype

    def toJSON(self):
        return json.dumps(self,
                          default=lambda o: o.__dict__,
                          sort_keys=True,
                          indent=4)


class User(pfmodelclasses.SystemEntity):
    """
	user entity
	"""

    def __init__(self, edf_id):
        self.json_schema_ref = "urn:eu.codecohe:user:1.0.0"
        self.edf_id = edf_id

    def setUser_id(self, user_id):
        """
		 type        string
		 description unique user ID (human readable) such as an email address
		"""
        self.user_id = user_id

    def setName(self, name):
        """
		 type        string
		 description The (human readable) name of the entity
		"""
        self.name = name

    def setDescription(self, description):
        """
		 type        string
		 description A description of the entity. May be empty.
		"""
        self.description = description

    def setIdentifier(self, identifier):
        """
		 type        string
		 description Name or other identifier of this entity. May be empty.
		"""
        self.identifier = identifier

    def setUrl(self, url):
        """
		 type        string
		 description URL that is used to locate this entity. May be empty.
		"""
        self.url = url

    def setType(self, type):
        """
		 type        string
		 description The more detailed type of the entity, e.g. RDBMS for a DataStore entity
		"""
        self.type = type

    def setSubtype(self, subtype):
        """
		 type        string
		 description The more detailed subtype of the entity, e.g. DB2 for an RDBMS type DataStore entity
		"""
        self.subtype = subtype

    def toJSON(self):
        return json.dumps(self,
                          default=lambda o: o.__dict__,
                          sort_keys=True,
                          indent=4)


class Workernode(pfmodelclasses.SystemEntity):
    """
	A worker node in a Kubernetes cluster
	"""

    def __init__(self, edf_id):
        self.json_schema_ref = "urn:eu.codecohe:workernode:1.0.0"
        self.edf_id = edf_id

    def setName(self, name):
        """
		 type        string
		 description The (human readable) name of the entity
		"""
        self.name = name

    def setDescription(self, description):
        """
		 type        string
		 description A description of the entity. May be empty.
		"""
        self.description = description

    def setIdentifier(self, identifier):
        """
		 type        string
		 description Name or other identifier of this entity. May be empty.
		"""
        self.identifier = identifier

    def setUrl(self, url):
        """
		 type        string
		 description URL that is used to locate this entity. May be empty.
		"""
        self.url = url

    def setType(self, type):
        """
		 type        string
		 description The more detailed type of the entity, e.g. RDBMS for a DataStore entity
		"""
        self.type = type

    def setSubtype(self, subtype):
        """
		 type        string
		 description The more detailed subtype of the entity, e.g. DB2 for an RDBMS type DataStore entity
		"""
        self.subtype = subtype

    def setClassification(self, classification):
        """
		 type        string
		 description The classification of the EDF.
		"""
        self.classification = classification

    def setClassification_value(self, classification_value):
        """
		 type        string
		 description An optional value of the classification
		"""
        self.classification_value = classification_value

    def setRegion(self, region):
        """
		 description The region.
		"""
        self.region = region

    def setCountry_code(self, country_code):
        """
		 type        string
		 description ISO 3166-1 alpha-2 two letter code
		"""
        self.country_code = country_code

    def setCity(self, city):
        """
		 type        string
		 description Optional name of the city or area
		"""
        self.city = city

    def setTags(self, tags):
        """
		 type        array
		 description An array of tags associated with this entity.
		"""
        self.tags = tags

    def setVersion(self, version):
        """
		 type        string
		 description The full version, can be a maven group/artifact/version string.
		"""
        self.version = version

    def setGit_commit_id(self, git_commit_id):
        """
		 type        string
		 description The git commit id.
		"""
        self.git_commit_id = git_commit_id

    def setGit_build_version(self, git_build_version):
        """
		 type        string
		 description The git build version.
		"""
        self.git_build_version = git_build_version

    def setVersion_url(self, version_url):
        """
		 type        string
		 description The URL where more information can be found, e.g. a doc webpage, a git repository.
		"""
        self.version_url = version_url

    def setStatus(self, status):
        """
		 type        string
		 description The status of the entity.
		"""
        self.status = status

    def toJSON(self):
        return json.dumps(self,
                          default=lambda o: o.__dict__,
                          sort_keys=True,
                          indent=4)


class Workload(pfmodelclasses.SystemEntity):
    """
	A workload running in Kubernetes
	"""

    def __init__(self, edf_id):
        self.json_schema_ref = "urn:eu.codecohe:workload:1.0.0"
        self.edf_id = edf_id

    def setName(self, name):
        """
		 type        string
		 description The (human readable) name of the entity
		"""
        self.name = name

    def setDescription(self, description):
        """
		 type        string
		 description A description of the entity. May be empty.
		"""
        self.description = description

    def setIdentifier(self, identifier):
        """
		 type        string
		 description Name or other identifier of this entity. May be empty.
		"""
        self.identifier = identifier

    def setUrl(self, url):
        """
		 type        string
		 description URL that is used to locate this entity. May be empty.
		"""
        self.url = url

    def setType(self, type):
        """
		 type        string
		 description The more detailed type of the entity, e.g. RDBMS for a DataStore entity
		"""
        self.type = type

    def setSubtype(self, subtype):
        """
		 type        string
		 description The more detailed subtype of the entity, e.g. DB2 for an RDBMS type DataStore entity
		"""
        self.subtype = subtype

    def setClassification(self, classification):
        """
		 type        string
		 description The classification of the EDF.
		"""
        self.classification = classification

    def setClassification_value(self, classification_value):
        """
		 type        string
		 description An optional value of the classification
		"""
        self.classification_value = classification_value

    def setPurpose(self, purpose):
        """
		 type        array
		 description An array of purposes.
		"""
        self.purpose = purpose

    def setTags(self, tags):
        """
		 type        array
		 description An array of tags associated with this entity.
		"""
        self.tags = tags

    def setVersion(self, version):
        """
		 type        string
		 description The full version, can be a maven group/artifact/version string.
		"""
        self.version = version

    def setGit_commit_id(self, git_commit_id):
        """
		 type        string
		 description The git commit id.
		"""
        self.git_commit_id = git_commit_id

    def setGit_build_version(self, git_build_version):
        """
		 type        string
		 description The git build version.
		"""
        self.git_build_version = git_build_version

    def setVersion_url(self, version_url):
        """
		 type        string
		 description The URL where more information can be found, e.g. a doc webpage, a git repository.
		"""
        self.version_url = version_url

    def toJSON(self):
        return json.dumps(self,
                          default=lambda o: o.__dict__,
                          sort_keys=True,
                          indent=4)


class Complies(pfmodelclasses.SystemRelationship):
    """
	A complies relationship.
	"""

    def __init__(self, from_edf_id, to_edf_id):
        self.json_schema_ref = "urn:eu.codecohe:complies:1.0.0"
        self.from_edf_id = from_edf_id
        self.to_edf_id = to_edf_id

    def toJSON(self):
        return json.dumps(self,
                          default=lambda o: o.__dict__,
                          sort_keys=True,
                          indent=4)


class Contains(pfmodelclasses.SystemRelationship):
    """
	A contains relationship.
	"""

    def __init__(self, from_edf_id, to_edf_id):
        self.json_schema_ref = "urn:eu.codecohe:contains:1.0.0"
        self.from_edf_id = from_edf_id
        self.to_edf_id = to_edf_id

    def toJSON(self):
        return json.dumps(self,
                          default=lambda o: o.__dict__,
                          sort_keys=True,
                          indent=4)


class Exposed_to(pfmodelclasses.SystemRelationship):
    """
	An exposed to relationship.
	"""

    def __init__(self, from_edf_id, to_edf_id):
        self.json_schema_ref = "urn:eu.codecohe:exposed_to:1.0.0"
        self.from_edf_id = from_edf_id
        self.to_edf_id = to_edf_id

    def toJSON(self):
        return json.dumps(self,
                          default=lambda o: o.__dict__,
                          sort_keys=True,
                          indent=4)


class Has(pfmodelclasses.SystemRelationship):
    """
	A has relationship.
	"""

    def __init__(self, from_edf_id, to_edf_id):
        self.json_schema_ref = "urn:eu.codecohe:has:1.0.0"
        self.from_edf_id = from_edf_id
        self.to_edf_id = to_edf_id

    def toJSON(self):
        return json.dumps(self,
                          default=lambda o: o.__dict__,
                          sort_keys=True,
                          indent=4)


class Owns(pfmodelclasses.SystemRelationship):
    """
	An owns relationship.
	"""

    def __init__(self, from_edf_id, to_edf_id):
        self.json_schema_ref = "urn:eu.codecohe:owns:1.0.0"
        self.from_edf_id = from_edf_id
        self.to_edf_id = to_edf_id

    def toJSON(self):
        return json.dumps(self,
                          default=lambda o: o.__dict__,
                          sort_keys=True,
                          indent=4)


class Reads(pfmodelclasses.SystemRelationship):
    """
	A reads relationship.
	"""

    def __init__(self, from_edf_id, to_edf_id):
        self.json_schema_ref = "urn:eu.codecohe:reads:1.0.0"
        self.from_edf_id = from_edf_id
        self.to_edf_id = to_edf_id

    def toJSON(self):
        return json.dumps(self,
                          default=lambda o: o.__dict__,
                          sort_keys=True,
                          indent=4)


class Runs(pfmodelclasses.SystemRelationship):
    """
	A runs relationship.
	"""

    def __init__(self, from_edf_id, to_edf_id):
        self.json_schema_ref = "urn:eu.codecohe:runs:1.0.0"
        self.from_edf_id = from_edf_id
        self.to_edf_id = to_edf_id

    def toJSON(self):
        return json.dumps(self,
                          default=lambda o: o.__dict__,
                          sort_keys=True,
                          indent=4)


class Stores(pfmodelclasses.SystemRelationship):
    """
	A stores relationship.
	"""

    def __init__(self, from_edf_id, to_edf_id):
        self.json_schema_ref = "urn:eu.codecohe:stores:1.0.0"
        self.from_edf_id = from_edf_id
        self.to_edf_id = to_edf_id

    def toJSON(self):
        return json.dumps(self,
                          default=lambda o: o.__dict__,
                          sort_keys=True,
                          indent=4)


class Writes(pfmodelclasses.SystemRelationship):
    """
	A writes relationship.
	"""

    def __init__(self, from_edf_id, to_edf_id):
        self.json_schema_ref = "urn:eu.codecohe:writes:1.0.0"
        self.from_edf_id = from_edf_id
        self.to_edf_id = to_edf_id

    def toJSON(self):
        return json.dumps(self,
                          default=lambda o: o.__dict__,
                          sort_keys=True,
                          indent=4)
