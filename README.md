<!--
  ~ SPDX-License-Identifier: Apache-2.0
  ~ Copyright IBM Corp 2023
-->

# Connectors

Connectors send metadata to MDM in the form of events. Events are structured JSON documents following [this JSON schema](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/metadata-manager-mdm/graphdb/-/tree/main/schemas/eu.codecohe?ref_type=heads).

MDM Connector developers can leverage [these classes](metadata-model/src/main/python/mdmmodelclasses.py) to generate the JSON events from the metadata gathered.

MDM connectors are based on IBM's Open Source [Pathfinder SDK](https://github.com/IBM/pathfinder-python-sdk). Please find there more detailed documentation.

## CODECO default Connectors

### k8s connector

edit/create your values.yaml
- Update api_url with the correct kubernetes API URL.
- The _url_ is here the internal service, replace `<namespace>` with the install namspace or for multicluster the full ingress url.
```yaml
crawler:
  k8s:
    api_url: https://kubernetes.default.svc 
pathfinder:
  url: http://mdm-api.<namespace>:8090/mdm/api/v1
  connector:
    id: k8sconnector-<your clustername>
```
helm --kube-context=$MDM_CONTEXT -n $MDM_NAMESPACE install k8s-connector ./connectors/k8s/src/main/helm [ -f values.yaml ]

### kubescape connector

edit/create your values.yaml
- Update api_url with the correct kubernetes API URL.
- The _url_ is here the internal service, replace `<namespace>` with the install namspace or for multicluster the full ingress url.
```yaml
crawler:
  k8s:
    api_url: https://kubernetes.default.svc 
pathfinder:
  url: http://mdm-api.<namespace>:8090/mdm/api/v1 
  connector:
    id: kubescape-<your clustername>
```
helm --kube-context=$MDM_CONTEXT -n $MDM_NAMESPACE install kubescape-connector ./connectors/kubescape/src/main/helm [ -f values.yaml ]

### prometheus connector
edit/create your values.yaml
- Update api_url with the correct kubernetes API URL.
- The _url_ is here the internal service, replace `<namespace>` with the install namspace or for multicluster the full ingress url.
- Update the prometheus url and port
- metric of the freshness metric
- schedule here e.g. update every 60 second
```yaml
crawler:
  k8s:
    api_url: https://kubernetes.default.svc 
  prometheus:
    server:
      url: http://<prometheus service>.<namespace>.svc.cluster.local
      port: 9090
    metric: <freshness metric name>
  schedule: 60

pathfinder:
  url: http://mdm-api.<namespace>:8090/mdm/api/v1 
  connector:
    id: kubescape-<your clustername>

```
helm --kube-context=$MDM_CONTEXT -n $MDM_NAMESPACE install freshness-connector ./connectors/prometheus/src/main/helm [ -f values.yaml ]

## Installation of the SDK
To install, use `pip`:

```shellscript
python -m pip install --upgrade ibm_pathfinder_sdk
```


## Using the SDK

To start a simple MDM connector, set up you Python environment and start with a `my_connector.py` file:

```python
import uuid
import json
from ibm_pathfinder_sdk import eventpublisher as msgObject , pathfinderconfig, pfmodelclasses
from mdmmodelclasses import Kubernetes, Workernode, Contains

class NULL_NAMESPACE:
    bytes = b''

UNIQUE_CONNECTOR_ID = "my-connector-test01"
PATHFINDER_CONNECTOR_STOP_SIGNAL = "GO"

# use pf-model-registry
conMsgObject = msgObject.ConMsgObjectModelRegistry()
# If you like to write the events to stdout only, 
# because you don't have a running Pathfinder backend/registry, 
# use ConMsgObjectBase() instead. 
# conMsgObject =  msgObject.ConMsgObjectBase()

connectorUuid = str(uuid.uuid3(NULL_NAMESPACE, UNIQUE_CONNECTOR_ID))
connectorType = "PYTHON_EXAMPLE"
event = pfmodelclasses.SystemEvent(connectorUuid, connectorType)



kubernetesName = "Kubernetes 001"
kubernetesEdfId = (str(uuid.uuid3(NULL_NAMESPACE,str(kubernetesName))))

k8sObj = Kubernetes(kubernetesEdfId)
k8sObj.setName(kubernetesName)
k8sObj.setDescription("My demo Kubernetes")

event.payload = k8sObj
# event setting, default is upsert
# event.event_type = "upsert"
# event.event_type = "delete" 
conMsgObject.publishEvent(event)


for i in range(1, 11):
    workerName = str("Worker-" + str(i))
    workerEdfId = (str(uuid.uuid3(NULL_NAMESPACE, workerName)))
    workerObj = Workernode(workerEdfId)
    workerObj.setName(workerName) 
    event.payload = workerObj
    conMsgObject.publishEvent(event)

    containsObj = Contains(kubernetesEdfId, workerEdfId)
    event.payload = containsObj
    conMsgObject.publishEvent(event)    

```

### Configuration
The configuration can be done in two variants, either environment variables or file-based.

To do it with environmental variables, export the variables and start python:
```shellscript
export OIDC_CLIENT_ENABLED=False
export K8S_TOKEN_AUTH=False
# if you use msgObject.ConMsgObjectBase(), you dont need this export
export PF_MODEL_REGISTRY_URL=http://<registry-url>:<port>/<api path>
# start your connector
python my_connector.py --env
```
To use a configuration file instead, create it as `config/application.yaml`.
This are the minimal parameters:
```yaml
stopMode: stop

pathfinder:
  kubernetesUrl: https://<cluster>:<port>
  url: http://<registry-url>:<port>/<api path>
  connector:
    state:
      type: local

k8sauth:
  enabled: False
oidc:
  enabled: False
```
Finally you can run the connector :
```shellscript
python my_connector.py
```

## License

MDM connectors are licensed under the Apache-2.0 license.

IBM's Pathfinder SDK for Python is licensed under the Apache-2.0 license.

The license full text can be found in [LICENSE](./LICENSE).